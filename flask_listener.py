#!/usr/bin/env python

from flask import Flask
from flask import request
from flask import render_template
from flask import redirect, url_for

from raspipe import RasPipe

import urllib2
import json
import time
import threading

f = urllib2.urlopen('http://freegeoip.net/json/')
json_string = f.read()
f.close()
location = json.loads(json_string)

#current zip code
global zipc 
zipc = location['zip_code']
#status of window
global window_closed 
window_closed = 1
#if user wants to close window
global user_close 
user_close = 0
#access wunderground
global f 
f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+zipc+'.json')
global json_string
json_string = f.read()
global parsed_json
parsed_json = json.loads(json_string)
#chance of rain
global pop
precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
#weather forecast
global forecast
weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']

app = Flask(__name__)

rp = RasPipe(None)
rp.input_lines.append('starting up...')
rp.render_frame()

#start webpage
@app.route('/')
def index():
    return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)

#get zip code
@app.route('/send', methods=['POST'])
def send():
    if len(request.form['address']) == 5 and request.form['address'].isdigit():
        global zipc
        zipc = int(request.form['address'])
        global f
        f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+ request.form['address'] +'.json')
        global json_string
        json_string = f.read()
        global parsed_json
        parsed_json = json.loads(json_string)
        global precipitation
        precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
        global weather
        weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']
        rp.input_lines.append(request.form['address'])
        rp.render_frame()
        return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))

#get button pressing
@app.route('/window', methods=['POST'])
def window():
    if request.method == 'POST':
        if request.form['submit'] == 'Open':
            global user_close
            user_close = 0
            global window_closed
            window_closed = 0
            rp.input_lines.append('Open')
            rp.render_frame()
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
        elif request.form['submit'] == 'Close':
            global user_close
            user_close = 1
            global window_closed
            window_closed = 1
            rp.input_lines.append('Close')
            rp.render_frame()
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))

def check_weather():
    print ("hi")
    while(1):
        global window_closed
        if(window_closed == 0):
            global zipc
            print zipc
            global weather
            print weather
            global precipitation
            print precipitation
            if(weather == "Rain" or weather == "Sleet" or weather == "Snow" or weather == "Flurries" or weather == "Thunderstorm"):
                print("We are closing the window!")
                global window_closed
                window_closed = 1;
            else:
                print("We are checking again to see if the weather changes!")        

            if(precipitation > 90):
                #time.sleep(300)
                print ("precipitation > 90")
                time.sleep(1)
            elif(precipitation <= 90 and precipitation > 75):
                #time.sleep(900)
                print ("precipitation <= 90 and precipitation > 75")
                time.sleep(5)
            elif(precipitation <= 75 and precipitation > 50):
                #time.sleep(1800)
                print ("precipitation <= 75 and precipitation > 50")
                time.sleep(10)
            elif(precipitation <= 50 and precipitation >= 0):
                #time.sleep(3600)
                print ("precipitation <= 50 and precipitation >= 0")
                time.sleep(15)

#main
if __name__ == '__main__':
    check_thread = threading.Thread(target = check_weather)
    check_thread.start()
    app.run(host='0.0.0.0')