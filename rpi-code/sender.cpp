#include <cstdlib>
#include <iostream>
#include<string.h> //strcpy
#include <sstream> //sleep()
#include <unistd.h>
#include <string>
#include <RF24/RF24.h>

//#include <json/json.h>
using namespace std;

//RF24 radio(RPI_V2_GPIO_P1_15, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_16MHZ);
RF24 radio(22,0);
int radioNumber  = 1;
const uint8_t address[][6] = {"1Node","2Node"};

struct payload{
	short action;
	char result[8];
}message;

void setup(){
    //Setup message
    message.action = 25;
    strcpy(message.result, "failure");
    
    //Setup radio
   radio.begin();
   radio.setAutoAck(1);
   radio.setRetries(15,15);
   
    if ( radioNumber )    {
      radio.openWritingPipe(address[1]);
      radio.openReadingPipe(1,address[0]);
    } else {
      radio.openWritingPipe(address[0]);
      radio.openReadingPipe(1,address[1]);
    }
    radio.startListening();
    radio.printDetails();
    
}

int main(int argc, char** argv)
{
    setup();
    ;
   // message.result = "failure";
    //Loops forever
     while(1){
     	//Stop listening to be able to send messages
     	radio.stopListening();
     
     	 //Checks if radio sent
     	 bool success =  radio.write(&message, sizeof(message));
     	
     	 
     	 //Wait here until we get a response, or timeout (250ms)
			unsigned long started_waiting_at = millis();
			bool timeout = false;
			while ( ! radio.available() && ! timeout ) {
				if (millis() - started_waiting_at > 250 )
					timeout = true;
			}
			
			
			// Describe the results
			if ( timeout )
			{
				printf("Failed, response timed out.\n");
			}
			else
			{
				// Grab the response, compare, and send to debugging spew
				unsigned long got_time;
				radio.read( &message, sizeof(message) );
				printf("Message response : %s\n" , message.result);

				// Spew it
				//printf("Got response %lu, round-trip delay: %lu\n",got_time,millis()-got_time);
			}
			sleep(1);
     	 
     	 /*Doesn't work for some reason
     	 if(!success)
     	 	printf("Radio failed to send messge\n"); */
     	 	
 	 	radio.startListening();
 	 	 
     	 	
     }
}

