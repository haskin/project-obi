#!/usr/bin/env python

#Imports for the nrf24l01 transciever 
 #Makes in compatible with Python 3 print()
from __future__ import print_function

#Sys used to find of the size of message when using radio.write()
import sys

import time                                                                          
from RF24 import *

#Imports for the flask webserver & weather
from flask import Flask
from flask import request
from flask import render_template
from flask import redirect, url_for
 
from raspipe import RasPipe
 
import urllib2
import json
import time
import threading
 
f = urllib2.urlopen('http://freegeoip.net/json/')
json_string = f.read()
f.close()
location = json.loads(json_string)
 
#current zip code
zipc = location['zip_code']
#status of window
window_closed = 0
#if user wants to close window
user_close = 0
#access wunderground
f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+zipc+'.json')
json_string = f.read()
parsed_json = json.loads(json_string)
#chance of rain
precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
#weather forecast
weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']
 
app = Flask(__name__)
 
rp = RasPipe(None)
rp.input_lines.append('starting up...')
rp.render_frame()
 
 #Radio settings
radio = RF24(22,0)

#Determines if the RPi will be recieving or sending calls
#radio_number(1) =  sending, radio_number(0) = revieving
radio_number = '1' 

#Sets up pipes that are used for communication
#address = ["Node1", "Node2"]
address = [0xF0F0F0F0E1, 0xF0F0F0F0D2]

#stuct payload
#close_window = 1
#open_window = 0
#1 to close the window, 0 to open it
message = '01'

#Boolean value that keeps track if the window is closed
#or open. If False the window is open. We assume the window
#starts out open, to begin tacking weather.
#window_closed = 0


millis = lambda: int(round(time.time() * 1000))

#Setting up radio
radio.begin()
radio.enableDynamicPayloads()
#radio.setAutoAck(1)
radio.setRetries(5,15)

if radio_number == '0':
    radio.openWritingPipe(address[1])
    radio.openReadingPipe(1,address[0])
elif radio_number == '1':
    radio.openWritingPipe(address[0])
    radio.openReadingPipe(1, address[1])
else :
    print("Radio number wasn't chosen properly")
    exit()

radio.startListening()
radio.printDetails()

#start webpage
@app.route('/')
def index():
    return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
 
#get zip code
@app.route('/send', methods=['POST'])
def send():
    if len(request.form['address']) == 5 and request.form['address'].isdigit():
        global zipc
        zipc = int(request.form['address'])
        global f
        f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+ request.form['address'] +'.json')
        global json_string
        json_string = f.read()
        global parsed_json
        parsed_json = json.loads(json_string)
        global precipitation
        precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
        global weather
        weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']
        rp.input_lines.append(request.form['address'])
        rp.render_frame()
        return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))
 
#get button pressing
@app.route('/window', methods=['POST'])
def window():
    if request.method == 'POST':
        if request.form['submit'] == 'Open':
            global user_close
            user_close = 0
            global window_closed
            window_closed = 0
            rp.input_lines.append('Open')
            rp.render_frame()
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
        elif request.form['submit'] == 'Close':
            global user_close
            user_close = 1
            global window_closed
            window_closed = 1
            rp.input_lines.append('Close')
            rp.render_frame()
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))
 
def check_weather():
	while(1):		
		print()
		#print("Value of window_closed is={}".format(window_closed))
		radio.startListening()
		#global window_closed
		#Check to see if recieving message from Obi Body   
		if(radio.available()):
			#Whlie getting message about window
			while (radio.available()):
				#print("Hello world")
				len = radio.getDynamicPayloadSize()
				#print("Hello world1")
				if len:
					#global window_closed
					#print("Hello world2")
					#print("Radio read {}".format(radio.read(len)))
					recieved_message = radio.read(len)
					print("Value in message {}".format(recieved_message))
					global window_closed
					if( recieved_message == "op" ):
						window_closed = 0
						print("New value in window_closed is={}".format(window_closed))
					elif(recieved_message == "cl"):
						window_closed = 1
						print("New value in window_closed is={}".format(window_closed))
					#TODO: remove this later
					#time.sleep(5)		
				else:
					continue
				
        #global window_closed
        time.sleep(5)
        if not(window_closed):
			global zipc
			print(zipc)
			global weather
			print(weather)
			global precipitation
			print(precipitation)

			precipitation = int((parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop']))
			weather = (parsed_json['forecast']['txt_forecast']['forecastday'][0]['title'])

			#TODO: comment the weather out
			weather = "Rain";
			if(weather == "Rain" or weather == "Sleet" or weather == "Snow" or weather == "Flurries" or weather == "Thunderstorm"):
				print("We are closing the window!")
				global window_closed
				radio.stopListening();

				print("Sending radio message : {}".format(message[close_window]))
				radio.write(message[close_window])

				#Start listening again to get a response back
				radio.startListening()

			else:
				print("We are checking again to see if the weather changes!")        
				if(precipitation > 90):
					#time.sleep(300)
					print ("precipitation > 90")
					time.sleep(1)
				elif(precipitation <= 90 and precipitation > 75):
					#time.sleep(900)
					print ("precipitation <= 90 and precipitation > 75")
					time.sleep(5)
				elif(precipitation <= 75 and precipitation > 50):
					#time.sleep(1800)
					print ("precipitation <= 75 and precipitation > 50")
					time.sleep(10)
				elif(precipitation <= 50 and precipitation >= 0):
					#time.sleep(3600)
					print ("precipitation <= 50 and precipitation >= 0")
					time.sleep(15)
	elif(window_closed):
		print("The window is currently closed"	)
	else:
		print("Window position unknown")

#main
if __name__ == '__main__':
    check_thread = threading.Thread(target = check_weather)
    check_thread.start()
    app.run(host='0.0.0.0')
