#include <Servo.h>

/*Possible problems : 
I think we may need to have an interrupt look at the toggleSwitch.
Also use an interrupt to track stallPin in A0. If stallPin is HIGH terminate everything.
*/
//Button pins
int openButton = A2;
int closeButton = A1;

// Determines which way motor spins
	
int toggleSwitch = 2;
//IF 0 spins clockwise
//IF 1 spins count-clockwise
int toggleValue;

Servo myServo;

//Stall Pin (Determines if Motor is stalling)
int stallPin = A3; 

void setup() {
	//Motor pins
	pinMode(toggleSwitch, INPUT);

	//Button pins
	pinMode(openButton, INPUT);
	pinMode(closeButton, INPUT);
	pinMode(stallPin, INPUT);
  myServo.attach(A0);
  myServo.write(91);

  Serial.begin(9600);
}

void loop() {
    toggleValue = digitalRead(toggleSwitch);
    Serial.println(digitalRead(closeButton));
    
	  while(digitalRead(openButton) == LOW && analogRead(stallPin) > 1020)
	    openWindow();

    //while(digitalRead(closeButton) == LOW)
      //closeWindow();

  // The read is wrong, it is suppose to read the signal from the raspberry pi
    if(digitalRead(closeButton) == LOW){
      closeFully();
      delay(500);
      while( analogRead(stallPin) > 1020 ) {
        closeFully();
      }
      myServo.write(91);
    }
    myServo.write(91);
    Serial.println(analogRead(stallPin));
    delay(500);
}

//Opens the window
void openWindow(){
    if(toggleValue == 0)
    	myServo.write(180);
    else
      myServo.write(0);
    delay(1000);
    Serial.println(analogRead(stallPin));
}

//Closes the window
void closeWindow(){
    if(toggleValue == 0)
      myServo.write(0);
    else
      myServo.write(180);
    delay(1000);
    Serial.println(analogRead(stallPin));
}

void closeFully(){
     if(toggleValue == 0)
      myServo.write(0);
    else
      myServo.write(180);
}
/*Turn motor one way while button pressed and motor not staled
whil(digitalRead(switch1) == HIGH && sigPin == LOW){
  digitalWrite(motorPin1, LOW);
  digitalWrite(motorPin2, HIGH);
}
//Turn motor other way while other button pressed and motor not stallled
while(digitalRead(switch2) == HIGH && sigPin == LOW){
  digitalWrite(motorPin1, HIGH);
  digitalWrite(motorPin2, LOW);
}*/
