#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

int msg[1];
RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;

/*
//Send signal
void setup(void){
 Serial.begin(9600);
 radio.begin();
 radio.openWritingPipe(pipe);
}

void loop(void){
 if (Serial.available() > 0) {
  msg[0] = 111;
  radio.write(msg, 1);
  Serial.println("Message sent.");
  int data = Serial.read();
  Serial.println(data, DEC);
 }
}
*/

//Recieve signal
void setup(void){
 Serial.begin(9600);
 delay(1000);
 Serial.println("Nrf24L01 Receiver Starting");
 radio.begin();
 radio.openReadingPipe(3,pipe);
 radio.startListening();
}

void loop(void){
 if (radio.available()){
   Serial.println("Radio is available");
   bool done = false;    
   while (!done){
     done = radio.read(msg, 1);      
     Serial.println(msg[0]);
     if (msg[0] == 111){
      Serial.println("Signal recieved.");
     }
     else {
      Serial.println("Signal not recived.");
     }
     delay(50);
   }
 }
 else{
  Serial.println("No radio available");
  delay(50);
 }
}

