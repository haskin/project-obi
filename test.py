import urllib2
import json
import time

#Find zip code usin website based on ip address
f = urllib2.urlopen('http://freegeoip.net/json/')
json_string = f.read()
f.close()
location = json.loads(json_string)
#print(location)
#Assign zip code
location_city = location['city']
location_state = location['region_name']
location_country = location['country_name']
location_zip = location['zip_code']

#Use webste to automatically get ip address and weather
f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+location_zip+'.json')
json_string = f.read()
parsed_json = json.loads(json_string)
location = parsed_json['location']['city']
temp_f = parsed_json['current_observation']['temp_f']
#pop = parsed_json['current_observation']['precip_today']
print "Current temperature in %s is: %s" % (location, temp_f)
#print "Current precipitation in %s is: %s" % (location, pop)
print("percip_today_string:"+parsed_json['current_observation']['precip_today_string'])
print("percip_today_metric:"+parsed_json['current_observation']['precip_today_metric'])
print("percip_today_in:"+parsed_json['current_observation']['precip_today_in'])
print("precip_1hr_string:"+parsed_json['current_observation']['precip_1hr_string'])
print("precip_1hr_metric:"+parsed_json['current_observation']['precip_1hr_metric'])
print("precip_1hr_in:"+parsed_json['current_observation']['precip_1hr_in'])
print("pop: " + parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
print("forecast: " + parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon'])
print("fcttext: " + parsed_json['forecast']['txt_forecast']['forecastday'][0]['fcttext'])
print("fcttext_metric: " + parsed_json['forecast']['txt_forecast']['forecastday'][0]['fcttext_metric'])
print("title: " + parsed_json['forecast']['txt_forecast']['forecastday'][0]['title'])
print("city: " + parsed_json['location']['city'])
print("city " + parsed_json['location']['zip'])

window_closed = 0

while(1):
    if(window_closed == 0):
        
        precipitation = int((parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop']))
        weather = (parsed_json['forecast']['txt_forecast']['forecastday'][0]['title'])

        if(weather == "Rain" or weather == "Sleet" or weather == "Snow" or weather == "Flurries" or weather == "Thunderstorm"):
            print("We are closing the window!")
            window_closed = 1;
        else:
            print("We are checking again to see if the weather changes!")        

        if(precipitation > 90):
            #time.sleep(300)
            time.sleep(1)
        elif(precipitation <= 90 and precipitation > 75):
            #time.sleep(900)
            time.sleep(5)
        elif(precipitation <= 75 and precipitation > 50):
            #time.sleep(1800)
            time.sleep(10)
        elif(precipitation <= 50 and precipitation >= 0):
            #time.sleep(3600)
            time.sleep(15)

f.close()

